package thang.ngo.demotech.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.ExecutionException;

public interface IEmployeeService {
    List<String> getId();

    List<String> getListener() throws JsonProcessingException, ExecutionException, InterruptedException;
}
