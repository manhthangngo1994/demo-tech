package thang.ngo.demotech.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.requestreply.ReplyingKafkaTemplate;
import org.springframework.kafka.requestreply.RequestReplyFuture;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import springfox.documentation.spring.web.json.Json;
import thang.ngo.demotech.utils.Constants;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Service
public class EmployeeServiceImpl implements IEmployeeService {

    private final Logger logger = LoggerFactory.getLogger(EmployeeServiceImpl.class);

    private final KafkaTemplate kafkaTemplate;
    private final ReplyingKafkaTemplate replyingKafkaTemplate;

    public EmployeeServiceImpl(KafkaTemplate kafkaTemplate, ReplyingKafkaTemplate replyingKafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
        this.replyingKafkaTemplate = replyingKafkaTemplate;
    }

    @Override
    public List<String> getId() {
        long time = System.currentTimeMillis();
//        kafkaTemplate.send

//        ListenableFuture<SendResult<Long, String>> resultListenableFuture = kafkaTemplate.send("ngothang", 2, time, "Ngo Manh Thang");
//        resultListenableFuture.addCallback(new ListenableFutureCallback<SendResult<Long, String>>(){
//
//            @Override
//            public void onSuccess(SendResult<Long, String> longStringSendResult) {
//                logger.info("Gui thong tin len kafka thanh cong");
//            }
//
//            @Override
//            public void onFailure(Throwable throwable) {
//
//            }
//        });

        return Arrays.asList("Ngo Thang");
    }

    @Override
    public List<String> getListener() throws JsonProcessingException, ExecutionException, InterruptedException {
        String json = "{ \"id\": \"Baeldung\" }";
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode convertedObject1 = objectMapper.readTree(json);

        String key = String.valueOf(System.currentTimeMillis());

        // create producer record
        ProducerRecord<String, JsonNode> record = new ProducerRecord<>(Constants.KAFKA_CONFIG.TOPIC, 2, key ,convertedObject1);
        // set reply topic in header
        record.headers().add(new RecordHeader(KafkaHeaders.REPLY_TOPIC, Constants.KAFKA_CONFIG.TOPIC.getBytes()));
        record.headers().add(new RecordHeader(KafkaHeaders.REPLY_PARTITION, "1".getBytes()));
        record.headers().add(new RecordHeader(KafkaHeaders.MESSAGE_KEY, key.getBytes()));
        // post in kafka topic
        RequestReplyFuture<String, JsonNode, JsonNode> sendAndReceive = replyingKafkaTemplate.sendAndReceive(record);

        // confirm if producer produced successfully
        SendResult<String, JsonNode> sendResult = sendAndReceive.getSendFuture().get();
        ListenableFuture<SendResult<String, JsonNode>> listenableFuture = sendAndReceive.getSendFuture();

        //print all headers
        sendResult.getProducerRecord().headers().forEach(header -> System.out.println(header.key() + ":" + header.value().toString()));

        // get consumer record
        ConsumerRecord<String, JsonNode> consumerRecord = sendAndReceive.get();
        // return consumer value
        JsonNode jsonNode = consumerRecord.value();
        String result = objectMapper.writeValueAsString(jsonNode);
        return Arrays.asList(result);
    }

//    @KafkaListener(topics = "ngothang")
//    public void receive(String payload) {
//        logger.info("Thong tin nhan duoc: " + payload);
//    }
}
