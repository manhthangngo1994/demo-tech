package thang.ngo.demotech.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;
import thang.ngo.demotech.utils.Constants;

@Component
public class ListenerComponent {

    @KafkaListener(topicPartitions = {@TopicPartition(topic = Constants.KAFKA_CONFIG.TOPIC, partitions = "2")})
    @SendTo(Constants.KAFKA_CONFIG.TOPIC)
    public String listen(JsonNode json) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        String json1 = "{ \"id\": \"Baeldung11111\" }";
        return objectMapper.writeValueAsString(json);
    }
}
