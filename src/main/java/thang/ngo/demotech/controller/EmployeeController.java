package thang.ngo.demotech.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import thang.ngo.demotech.service.IEmployeeService;

import java.util.List;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("api/")
public class EmployeeController {
    private IEmployeeService employeeService;

    public EmployeeController(IEmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping(value = "get-id")
    public ResponseEntity<List<String>> getId(){
        List<String> list = employeeService.getId();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @GetMapping(value = "get-id-2")
    public ResponseEntity<List<String>> getId2() throws ExecutionException, JsonProcessingException, InterruptedException {
        List<String> list = employeeService.getListener();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }
}
